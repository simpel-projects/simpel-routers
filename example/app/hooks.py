from django.urls import include, path

import simpel_hookup.core as hookup

from simpel_routers.sites import AuthenticationRouter

from .views import OrderViewSet, ProductViewSet


@hookup.register("REGISTER_WEBSITE_PATH")
def register_auth_router():
    return path("accounts/", include(AuthenticationRouter().get_urls()))


@hookup.register("REGISTER_WEBSITE_VIEWSET")
def register_product_website_viewset():
    return ProductViewSet


@hookup.register("REGISTER_DASHBOARD_VIEWSET")
def register_order_dashboard_viewset():
    return OrderViewSet
