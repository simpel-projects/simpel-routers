from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.


class Product(models.Model):

    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Product"

    def __str__(self):
        return self.name


class Order(models.Model):

    user = models.ForeignKey(
        get_user_model(),
        related_name="orders",
        on_delete=models.CASCADE,
    )
    number = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Order"

    def __str__(self):
        return self.number
