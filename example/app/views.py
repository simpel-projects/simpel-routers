from simpel_routers.viewsets import ReadOnlyViewSet

from .models import Order, Product


class ProductViewSet(ReadOnlyViewSet):
    model = Product


class OrderViewSet(ReadOnlyViewSet):
    model = Order
